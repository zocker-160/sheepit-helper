bl_info = {
    "name": "Sheep-It Helper",
    "author": "Finn Bear",
    "location": "Farm",
    "version": (1, 1, 1),
    "blender": (2, 80, 0),
    "description": "Prepares scene for upload to Sheep-It renderfarm.",
    "category": "Render",
    "tracker_url": "https://gitlab.com/finnbear/sheepit-helper/issues"
}

import os
import math
import bpy
from bpy.props import BoolProperty
from bpy.types import AddonPreferences
from bpy.types import Operator
from bpy.types import Panel

max_upload_size = 500 # hard limit # of megabytes
max_samples = 512 # soft limit # of smaples
max_ao_bounces = 5 # soft limit # of AO bounces
max_resolution = 18000 # hard limit # of pixels

name = "sheepit-helper-master"

# Read directly from the filesystem to determine the size of the project directory
zip_size = -1

def refresh_file_size():
    global zip_size

    zip_size_total = 0
    for dirpath, dirnames, filenames in os.walk(os.path.dirname(bpy.data.filepath)):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            if not os.path.islink(filepath):
                zip_size_total += os.path.getsize(filepath) * 0.000001
    zip_size = round(zip_size_total)

class SCENE_OT_sheepit_refresh_file_size(Operator):
    bl_idname = "scene.sheepit_refresh_file_size"
    bl_label = "Refresh"

    def execute(self, context):
        refresh_file_size()
        return {'FINISHED'}


# If camera is missing, find one and use it
class SCENE_OT_sheepit_fix_missing_active_camera(Operator):
    bl_idname = "scene.sheepit_fix_missing_active_camera"
    bl_label = "Fix"

    def execute(self, context):
        for scene in bpy.data.scenes:
            if scene.camera is None:
                for object in scene.objects:
                    if object.type == "CAMERA":
                        scene.camera = object
                        break

        return {'FINISHED'}

# Reduce AO bounces to recommended amount
class SCENE_OT_sheepit_fix_excessive_ao_bounces(Operator):
    bl_idname = "scene.sheepit_fix_ao_bounces"
    bl_label = "Fix"

    def execute(self, context):
        for scene in bpy.data.scenes:
            if scene.cycles is not None:
                scene.render.use_simplify = True
                if scene.cycles.ao_bounces_render == 0 or scene.cycles.ao_bounces_render > max_ao_bounces:
                    scene.cycles.ao_bounces_render = 3

        return {'FINISHED'}

# Remove alpha channel that can mess up video export
class SCENE_OT_sheepit_fix_rgba(Operator):
    bl_idname = "scene.sheepit_fix_rgba"
    bl_label = "Fix"

    def execute(self, context):
        for scene in bpy.data.scenes:
            if scene.render.image_settings.color_mode == "RGBA":
                scene.render.image_settings.color_mode = "RGB"

        return {'FINISHED'}

# Create a list of missing images to warn about
missing_images = []

def refresh_missing_images():
    global missing_images

    missing_images = []

    for image in bpy.data.images:
        path = image.filepath
        if path.startswith('//'):
            path = path[2:]
        if len(image.packed_files) == 0 and path != '' and not os.path.exists(os.path.join(os.path.dirname(bpy.data.filepath).encode('utf-8'), bpy.path.abspath(path).encode('utf-8'))):
            missing_images.append(image.filepath)

class SCENE_OT_sheepit_refresh_missing_images(Operator):
    bl_idname = "scene.sheepit_refresh_missing_images"
    bl_label = "Refresh"

    def execute(self, context):
        refresh_missing_images()
        return {'FINISHED'}

class RENDER_PT_sheepit_view(Panel):
    bl_label = "Sheep-It Helper Warnings"
    bl_region_type = "WINDOW"
    bl_space_type = "PROPERTIES"
    bl_context = "render"

    # Print a stand-alone line
    def print_item(self, layout, item, operator):
        box = layout.box()
        row = box.row()
        row.label(text=item)

        if operator != None:
            row = box.row()
            row.operator(operator.bl_idname, text=operator.bl_label)

    # Print a box with multiple lines
    def print_category(self, layout, header, items, operator):
        if len(items) > 0:
            box = layout.box()
            row = box.row()
            row.label(text=header)

            for item in items:
                row = box.row(align=True)
                row.label(text=" - " + item)

            if operator != None:
                row = box.row()
                row.operator(operator.bl_idname, text=operator.bl_label)

    # Warnings related to the blend file or directory
    def file_status(self, preferences):
        layout = self.layout
        status = 0

        # Not saving the blend file is a recipe for disaster, including absolute paths to images and bakes
        if bpy.data.is_saved:
            if zip_size > max_upload_size:
                if preferences.warn_file_excessive_size:
                    status = max(status, 1)
                    self.print_category(layout, "Warning: Project Directory Too Big (" + str(zip_size) + "/" + str(max_upload_size) + "MB)", ["Move the blend file to a directory with only essential files", "Purge orphaned data", "Optimize images and caches"], SCENE_OT_sheepit_refresh_file_size)

            if bpy.data.is_dirty:
                if preferences.warn_file_unsaved:
                    status = max(status, 1)
                    self.print_item(layout, "Warning: Unsaved Changes", None)
        else:
            if preferences.warn_file_unsaved:
                status = max(status, 1)
                self.print_category(layout, "Warning: Blend File Not Yet Saved", ["Imported images will not have proper paths", "Physics caches will not work"], None)

        return status

    # Warnings related to images imported into Blender
    def image_status(self, preferences):
        layout = self.layout
        status = 0

        unused_images = []
        absolute_path_images = []
        suboptimal_path_images = []
        unsupported_format_images = []
        unpacked_images = []

        for image in bpy.data.images:
            # This rules out things like render result
            if image.filepath == "":
                continue

            if preferences.warn_image_zero_users:
                if image.users == 0:
                    unused_images.append(image.filepath)

            # This exempts packed files from path checking
            if len(image.packed_files) > 0:
                continue

            # These patterns signify that the path is absolute and thus invalid
            if "//" not in image.filepath or "C:" in image.filepath:
                if preferences.warn_image_absolute_path:
                    status = max(status, 2)
                    absolute_path_images.append(image.filepath)
            else:
                if preferences.warn_image_unpacked:
                    unpacked_images.append(image.filepath)

            if preferences.warn_image_suboptimal_path:
                # These patterns signify that the path is suboptimal, but not necessarily invalid
                if ".." in image.filepath:
                    status = max(status, 1)
                    suboptimal_path_images.append(image.filepath)

            if preferences.warn_image_unsupported_format:
                if image.filepath.split(".")[-1].lower() in ["exr", "mov", "avi", "ogg"]:
                    unsupported_format_images.append(image.filepath)

        self.print_category(layout, "Warning: Unused Image", unused_images, None)
        self.print_category(layout, "Warning: Missing Image", missing_images, SCENE_OT_sheepit_refresh_missing_images)
        self.print_category(layout, "Error: Absolute Path", absolute_path_images, None)
        self.print_category(layout, "Warning: Problematic Path", suboptimal_path_images, None)
        self.print_category(layout, "Warning: Problematic File Format", unsupported_format_images, None)
        self.print_category(layout, "Info: ZIP Archive Necessary", unpacked_images, None)

        return status

    # Warnings related to objects in the scene
    def object_status(self, preferences):
        layout = self.layout
        status = 0

        mismatch_visibility_objects = []
        scripted_driver_objects = []

        for object in bpy.data.objects:
            if preferences.warn_object_mismatched_visibility:
                if object.hide_viewport != object.hide_render and object.type in ["MESH", "LIGHT"]:
                    status = max(status, 1)

                    if object.hide_viewport:
                        mismatch_visibility_objects.append(object.name + ": Only visible in renders")
                    else:
                        mismatch_visibility_objects.append(object.name + ": Will not appear in renders")

            if preferences.warn_object_scripted_driver:
                if object.animation_data != None:
                    for driver in object.animation_data.drivers:
                        if driver.driver != None:
                            if driver.driver.type == "SCRIPTED":
                                status = max(status, 2)
                                scripted_driver_objects.append(object.name + ": " + driver.data_path)

        self.print_category(layout, "Warning: Mismatched Visibility", mismatch_visibility_objects, None)
        self.print_category(layout, "Error: Scripted Driver (Scripts Blocked)", scripted_driver_objects, None)

        return status

    # Warnings specifically related to the scene
    def scene_status(self, preferences):
        layout = self.layout
        status = 0

        unsupported_renderer_scenes = []
        missing_camera_scenes = []
        missing_active_camera_scenes = []
        excessive_samples_scenes = []
        square_samples_scenes = []
        mismatch_samples_scenes = []
        excessive_ao_bounces_scenes = []
        excessive_resolution_scenes = []
        rgba_scenes = []
        enabled_sequencer_scenes = []

        for scene in bpy.data.scenes:
            samples = 0
            preview_samples = 0

            if scene.camera == None:
                status = max(status, 2)

                missing_camera = True
                for object in scene.objects:
                    if object.type == "CAMERA":
                        missing_camera = False
                        break

                if missing_camera:
                    if preferences.warn_scene_missing_camera:
                        missing_camera_scenes.append(scene.name)
                else:
                    if preferences.warn_scene_missing_active_camera:
                        missing_active_camera_scenes.append(scene.name)

            if scene.render.engine == "CYCLES":
                if scene.cycles.progressive == "PATH":
                    samples = scene.cycles.samples
                    preview_samples = scene.cycles.preview_samples
                else:
                    samples = scene.cycles.aa_samples
                    preview_samples = scene.cycles.preview_aa_samples

                # Square samples is often enabled by accident
                if scene.cycles.use_square_samples:
                    samples = samples * samples
                    preview_samples = preview_samples * preview_samples
                    if preferences.warn_scene_square_samples:
                        square_samples_scenes.append(scene.name)
                        status = max(status, 1)

                if preferences.warn_scene_excessive_ao_bounces:
                    # Technically, AO bounces are also limited by scene bounces
                    ao_bounces = min(scene.cycles.ao_bounces_render, scene.cycles.max_bounces) if scene.render.use_simplify else scene.cycles.max_bounces

                    # Zero AO bounces means infinite
                    if ao_bounces == 0:
                        ao_bounces = scene.cycles.max_bounces

                    if ao_bounces > max_ao_bounces:
                        status = max(status, 1)
                        excessive_ao_bounces_scenes.append(scene.name + ": " + str(ao_bounces) + " bounces")
            elif scene.render.engine == "BLENDER_EEVEE":
                samples = scene.eevee.taa_render_samples
                preview_samples = scene.eevee.taa_samples
            else:
                if preferences.warn_scene_unsupported_renderer:
                    unsupported_renderer_scenes.append(scene.name + ": " + scene.render.engine)
                    status = max(status, 2)

            if preferences.warn_scene_excessive_samples:
                # It is uncommon for high sample counts to be necessary
                if samples > max_samples:
                    excessive_samples_scenes.append(scene.name + ": " + str(samples) + " samples")
                    status = max(status, 1)

            if preferences.warn_scene_mismatched_samples:
                # It is uncommon for preview samples to exceed render samples
                if samples < preview_samples:
                    mismatch_samples_scenes.append(scene.name + ": Rendering fewer samples than viewport")

            if preferences.warn_scene_excessive_resolution:
                # There is a hard resolution limit in place
                if scene.render.resolution_x > max_resolution:
                    excessive_resolution_scenes.append(scene.name + ": " + str(scene.render.resolution_x) + " horizontal pixels")
                    status = max(status, 2)
                if scene.render.resolution_y > max_resolution:
                    excessive_resolution_scenes.append(scene.name + ": " + str(scene.render.resolution_y) + " vertical pixels")
                    status = max(status, 2)

            if preferences.warn_scene_rgba_color:
                # RGBA color causes playback problems for videos
                if scene.render.image_settings.color_mode == "RGBA":
                    rgba_scenes.append(scene.name)

            if preferences.warn_scene_enabled_sequencer:
                # Sequencer is not made more efficient by a renderfarm
                if scene.render.use_sequencer and scene.sequence_editor is not None and len(scene.sequence_editor.sequences_all) > 0:
                    enabled_sequencer_scenes.append(scene.name)

        self.print_category(layout, "Error: Unsupported Render Engine", unsupported_renderer_scenes, None)
        self.print_category(layout, "Error: Missing Camera", missing_camera_scenes, None)
        self.print_category(layout, "Error: Missing Active Camera", missing_active_camera_scenes, SCENE_OT_sheepit_fix_missing_active_camera)
        self.print_category(layout, "Warning: Excessive Samples (suggested max " + str(max_samples) + ")", excessive_samples_scenes, None)
        self.print_category(layout, "Warning: Square Samples", square_samples_scenes, None)
        self.print_category(layout, "Warning: Mismatched Samples", mismatch_samples_scenes, None)
        self.print_category(layout, "Info: Excessive AO Bounces", excessive_ao_bounces_scenes, SCENE_OT_sheepit_fix_excessive_ao_bounces)
        self.print_category(layout, "Warning: RGBA Color (final video unplayable)", rgba_scenes, SCENE_OT_sheepit_fix_rgba)
        self.print_category(layout, "Error: Excessive Resolution (max " + str(max_resolution) + ")", excessive_resolution_scenes, None)
        self.print_category(layout, "Info: Sequencer Enabled (use not recommended)", enabled_sequencer_scenes, None)

        return status

    def draw(self, context):
        layout = self.layout

        # 0 = no errors, 1 = warning, 2 = error
        status = 0
        row = layout.row()

        user_preferences = None

        try:
            user_preferences = context.preferences
        except:
            user_preferences = context.user_preferences

        if name in user_preferences.addons:
            addon_preferences = user_preferences.addons[name].preferences

            # Use the max function to pass through the highest level of warning
            status = max(status, self.file_status(addon_preferences))
            status = max(status, self.image_status(addon_preferences))
            status = max(status, self.object_status(addon_preferences))
            status = max(status, self.scene_status(addon_preferences))

            if status == 0:
                row.label(text="You're all set!")
            elif status == 1:
                row.label(text="Proceed to upload with caution.")
            else:
                row.label(text="You've got some work to do before uploading.")
        else:
            row.label(text="Could not load preferences.")

class RENDER_PT_sheepit_edit(Panel):
    bl_label = "Sheep-It Helper Optimization"
    bl_region_type = "WINDOW"
    bl_space_type = "PROPERTIES"
    bl_context = "render"

    def draw(self, context):
        layout = self.layout

class SheepitHelperPreferences(AddonPreferences):
    bl_idname = name
    #bl_idname = os.path.basename(os.path.dirname(__file__))

    warn_file_excessive_size = BoolProperty(name="Excessive Project Directory Size", default=True)
    warn_file_unsaved = BoolProperty(name="File Not Saved", default=True)

    warn_image_zero_users = BoolProperty(name="Unused Image", default=True)
    warn_image_missing_path = BoolProperty(name="Missing Image", default=True)
    warn_image_absolute_path = BoolProperty(name="Absolute Path", default=True)
    warn_image_suboptimal_path = BoolProperty(name="Problematic Path", default=True)
    warn_image_unsupported_format = BoolProperty(name="Problematic File Format", default=True)
    warn_image_unpacked = BoolProperty(name="ZIP Archive Necessary", default=True)

    warn_scene_unsupported_renderer = BoolProperty(name="Unsupported Render Engine", default=True)
    warn_scene_missing_camera = BoolProperty(name="Missing Camera", default=True)
    warn_scene_missing_active_camera = BoolProperty(name="Missing Active Camera", default=True)
    warn_scene_excessive_samples = BoolProperty(name="Excessive Samples", default=True)
    warn_scene_square_samples = BoolProperty(name="Square Samples", default=True)
    warn_scene_mismatched_samples = BoolProperty(name="Mismatched Samples", default=True)

    # The following is too obscure to enable by default
    warn_scene_excessive_ao_bounces = BoolProperty(name="Excessive AO Bounces", default=False)

    warn_scene_rgba_color = BoolProperty(name="RGBA Color", default=True)
    warn_scene_excessive_resolution = BoolProperty(name="Excessive Resolution", default=True)
    warn_scene_enabled_sequencer = BoolProperty(name="Sequencer Enabled", default=True)

    warn_object_mismatched_visibility = BoolProperty(name="Mismatched Visibility", default=True)
    warn_object_scripted_driver = BoolProperty(name="Scripted Driver", default=True)

    def print_category(self, layout, header, props):
        box = layout.box()
        row = box.row()
        row.label(text=header)

        row = box.row()
        row.split(factor=0.5)
        current_column = row.column()

        for i, prop in enumerate(props):
            current_row = current_column.row(align=True)
            current_row.prop(self, prop)

            if i + 1 == math.ceil(len(props) / 2):
                current_column = row.column()

    # Called by Blender when the properties panel is rerendered, such as when the user mouses over a property
    def draw(self, context):
        layout = self.layout

        self.print_category(layout, "Blend File Warnings", ["warn_file_excessive_size", "warn_file_unsaved"])
        self.print_category(layout, "Scene Warnings", ["warn_scene_unsupported_renderer", "warn_scene_missing_camera", "warn_scene_missing_active_camera", "warn_scene_excessive_samples", "warn_scene_square_samples", "warn_scene_mismatched_samples", "warn_scene_excessive_ao_bounces", "warn_scene_rgba_color", "warn_scene_excessive_resolution", "warn_scene_enabled_sequencer"])
        self.print_category(layout, "Image Warnings", ["warn_image_zero_users", "warn_image_missing_path", "warn_image_absolute_path", "warn_image_suboptimal_path", "warn_image_unsupported_format", "warn_image_unpacked"])
        self.print_category(layout, "Object Warnings", ["warn_object_mismatched_visibility", "warn_object_scripted_driver"])

# Saving the blend file may change the file sizse or the set of missing images
def post_save_refresh(param):
    refresh_file_size()
    refresh_missing_images()

# Helper function to unregister all handlers. even if there are duplicates
def unregister_handlers():
    handlers_to_remove = []

    for handler in bpy.app.handlers.save_post:
        if handler.__name__ == post_save_refresh.__name__:
            handlers_to_remove.append(handler)

    for handler_to_remove in handlers_to_remove:
        bpy.app.handlers.save_post.remove(handler_to_remove)

# Called by Blender to incorporate the addon
def register():
    bpy.utils.register_class(SheepitHelperPreferences);

    bpy.utils.register_class(SCENE_OT_sheepit_refresh_file_size)
    bpy.utils.register_class(SCENE_OT_sheepit_refresh_missing_images)
    bpy.utils.register_class(SCENE_OT_sheepit_fix_missing_active_camera)
    bpy.utils.register_class(SCENE_OT_sheepit_fix_excessive_ao_bounces)
    bpy.utils.register_class(SCENE_OT_sheepit_fix_rgba)

    bpy.utils.register_class(RENDER_PT_sheepit_view)
    #bpy.utils.register_class(RENDER_PT_sheepit_edit)

    unregister_handlers()

    bpy.app.handlers.save_post.append(post_save_refresh)

# Called by Blender to remove the addon
def unregister():
    #bpy.utils.unregister_class(RENDER_PT_sheepit_edit)
    bpy.utils.unregister_class(RENDER_PT_sheepit_view)

    bpy.utils.unregister_class(SCENE_OT_sheepit_refresh_file_size)
    bpy.utils.unregister_class(SCENE_OT_sheepit_refresh_missing_images)
    bpy.utils.unregister_class(SCENE_OT_sheepit_fix_missing_active_camera)
    bpy.utils.unregister_class(SCENE_OT_sheepit_fix_excessive_ao_bounces)
    bpy.utils.unregister_class(SCENE_OT_sheepit_fix_rgba)

    bpy.utils.unregister_class(SheepitHelperPreferences)

    unregister_handlers()

# This enables the addon to be tested in Blender's text editor via "Run Script"
if __name__ == "__main__":
    register()
