# Description

Sheep-It Helper is a helper addon for [Sheep-It Renderfarm](https://www.sheepit-renderfarm.com/)

![Preview Image 1](./preview-image-1.png)

# Instructions

1) Download the addon [Latest Version](https://gitlab.com/finnbear/sheepit-helper/-/archive/master/sheepit-helper-master.zip)

3) Open Blender 2.8

4) Edit -> Preferences -> Addons -> Install addon from file

5) Locate the zip file and install it

6) Look in the render tab of the properties panel

7) Address as many errors, warnings, and info messages as possible to maximize your chance of a successful render

# Development Instructions

1) Clone the repository

2) Open Blender 2.8

3) Open the Scripting workspace

4) Make cool changes

5) Press run script to test

6) Repeat steps 4-5

7) Push to the repository
